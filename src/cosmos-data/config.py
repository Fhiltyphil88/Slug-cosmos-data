# Database connection information
DB = {
    "name": "spacedao_stm_db",
    "host": "127.0.0.1",
    "port": "27017",
    "user": "root",
    "pass": "spaceda0"
}

# Database collections names
DB_COLS = {
    "cdm": "sd_cdm",
    "rso": "sd_rso"
}

# Connectors configuration for various supported space data providers
config_connectors = {
    "leolabs_public_daily": {
        "URL_base": "https://platform-cdn.leolabs.space",
        "data": "/conjunctions/visualization/overview/data"
    },
    "space_track_cdm": {
        "URL_base": "https://www.space-track.org",
        "auth_login": "/ajaxauth/login",
        "request_action": "/basicspacedata/query",
        "request_get_public_cdms": "/class/cdm_public"
    },
    "privateer_public_beta": {
        "URL_base": "https://preview.privateer-dev.com",
        "data_conjunction": "/data/conjunctions.json"
    }
}

cdm_changes = {
    "sample": {
        "CDM_ID": "null",
        "CREATED": "null",
        "SUPPLIED_ID": "null",
        "EVENT_ID": "null",
        "WHERE_LAT": "null",
        "WHERE_LON": "null",
        "TCA": "null",
        "MISS_DISTANCE": 0.0,
        "PC": "null",
    },
    "leolabs": {
        "probability": "PC",
        "tca": "TCA",
        "missDistance": "MISS_DISTANCE"
    },
    "privateer": {
        "__time": "CREATED",
        "cdmName": "CDM_ID",
        "collisionProbability2D": "PC",
        "distance": "MISS_DISTANCE"
    }
}
