# main functions of the cosmos data collector
from data_collector import cdm_from_space_track, \
    cdm_from_privateer, adapt_cdm_schema
from mongozor import insert_cdms
import typer

app = typer.Typer()


# routines

def leolabs_to_db() -> None:
    """ Collect all available CDMs from the LeoLabs Website.
    Store all collected CDMs in DB.
    """
    list_of_cdms = cdm_from_leolabs()
    corrected_list_of_cdms = adapt_cdm_schema(list_of_cdms, "leolabs")
    # insert_cdms(list_of_cdms)


def space_track_to_db() -> None:
    """ Collect all available CDMs from the Space Track APi.
    Store all collected CDMs in DB.
    """
    list_of_cdms = cdm_from_space_track()
    # corrected_list_of_cdms = adapt_cdm_schema(list_of_cdms, "spacetrack")
    insert_cdms(list_of_cdms)


def privateer_to_db() -> None:
    """ Collect all available CDMs from Privateer .
    Store all collected CDMs in DB.
    """
    list_of_cdms = cdm_from_privateer()
    corrected_list_of_cdms = adapt_cdm_schema(list_of_cdms, "privateer")
    insert_cdms(corrected_list_of_cdms)


# command lines
@app.command()
def get(source: str = typer.Argument("all",
        help="Which source to get, empty will get all")):
    if source in ["all", "spacetrack", "space_track", "space-track"]:
        space_track_to_db()
    if source in ["all", "privateer", "moriba"]:
        privateer_to_db()


if __name__ == "__main__":
    app()
