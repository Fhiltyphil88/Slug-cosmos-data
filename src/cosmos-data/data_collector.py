# This script collects the CDMs from different sources.
# Input: different CDM sources
# Output: CDMs in a dedicated SpaceDAO schema
# Included parameters:
#   cdm_id,
#   supplier_id,
#   event_id,
#   tca,
#   md,
#   created,
#   P_c,
#   where_lat,
#   where_lon


import requests
import json
from api_credentials import credentials
from config import config_connectors, cdm_changes


# extended exception handling
class MyConnectionError(Exception):
    def __init___(self, args):
        Exception.__init__(self,
                           "my exception was raised with arguments {0}"
                           .format(args))
        self.args = args


def cdm_from_leolabs() -> list:
    """ Get CDM data from LeoLabs
    """
    # space-track configuration
    data = None

    with requests.Session() as session:

        # receive all CDMs from the leolabs website
        response = session.get(
            config_connectors["leolabs_public_daily"]["URL_base"] +
            config_connectors["leolabs_public_daily"]["data"]
        )
        if response.status_code != 200:
            raise MyConnectionError(response,
                                    "GET fail on space track request")

        # extract data
        data = json.loads(response.text)

        session.close()
    return data


def cdm_from_space_track() -> list:
    """ Get CDM data from SpaceTrack providers
    """
    # space-track configuration
    data = None
    config_st_usr = credentials["space_track"]["username"]
    config_st_pwd = credentials["space_track"]["password"]
    site_credentials = {'identity': config_st_usr, 'password': config_st_pwd}

    # establish connection until closed with a session that saves authorization
    # tokens
    with requests.Session() as session:
        # test connection
        response = session.post(
            config_connectors["space_track_cdm"]["URL_base"] +
            config_connectors["space_track_cdm"]["auth_login"],
            data=site_credentials)
        if response.status_code != 200:
            raise MyConnectionError(response, "POST fail on login")

        # receive all CDMs from the space-track database
        # 401 failure means bad credentials
        response = session.get(
            config_connectors["space_track_cdm"]["URL_base"] +
            config_connectors["space_track_cdm"]["request_action"] +
            config_connectors["space_track_cdm"]["request_get_public_cdms"])
        if response.status_code != 200:
            raise MyConnectionError(response,
                                    "GET fail on space track request")

        # extract data
        data = json.loads(response.text)
        counter = len(data)
        print(counter)

        session.close()

    # every cdm in the space dao database needs:
    # cdm_id, supplier_id, event_id,
    # tca, md, created, P_c, where_lat, where_lon
    for cdm in data:
        cdm["SUPPLIED_ID"] = "st001"
        cdm["EVENT_ID"] = str(cdm["SAT_1_ID"])+str(cdm["SAT_2_ID"])
        cdm["WHERE_LAT"] = None
        cdm["WHERE_LON"] = None

    return data


def cdm_from_privateer() -> list:
    """ Get CDMs from privateer.

    Returns:
    list of dicts: all CDMs fitted to the seed_cdm schema
    """
    data = None  # dict of all collected CDms

    with requests.Session() as session:
        response = session.get(
            config_connectors["privateer_public_beta"]["URL_base"] +
            config_connectors["privateer_public_beta"]["data_conjunction"]
        )

        data = json.loads(response.text)

        # close request session
        session.close()

    return data


def adapt_cdm_schema(cdm_list: list, provider_name: str) -> list:
    """ Check and adapt the CDM schema coming from any provider.

    This is in order to have minimum set of features for each CDM used in our
    processing.

    :param cdm_list: list of CDMs as it is coming from the provider
    :paran provider_name: str name of the provider

    Return:
    dict Corrected CDMs
    """
    if provider_name in cdm_changes:
        # changes = cdm_changes["privateer"]
        changes = cdm_changes[provider_name]
        cdm_sample = cdm_changes["sample"]
    else:
        raise ValueError("Provider name is not supported: support@spacedao.ai")

    all_cdms = []

    # iterate over all CDMs
    for cdm in cdm_list:

        output = {}
        # Apply all changes stated in the change config json
        # Example: distance -> MISS_DISTANCE
        for k in changes:
            output[changes[k]] = cdm[k]

        # transfer the remaining items
        for k in [x for x in cdm if x not in changes]:
            output[k] = cdm[k]

        # add missing fields
        # for all keys in cdm_sample, but not in output, yet
        for k in [x for x in cdm_sample if x not in output]:
            output[k] = cdm_sample[k]

        all_cdms.append(output)
    return all_cdms


def match_events():
    pass


def dump_to_json(data: list, file_name: str) -> None:
    """ Output all CDM data to JSON.
    """
    with open(file_name, 'w', encoding='utf-8') as f:
        json.dump(data, f, ensure_ascii=False, indent=4)


# -- TEST: save cdms in json
# data = cdm_from_space_track()
#
# data = cdm_from_privateer()
# data = adapt_cdm_schema(data, "privateer")
# push_to_db()
# dump_to_json(data=data, file_name="__data__cdms.json")

# LeoLabs
# data = cdm_from_leolabs()
# data = adapt_cdm_schema(data, "leolabs")
# push_to_db()
# dump_to_json(data=data, file_name="__data__cdms.json")


# config_st_output = "__data__sd_all_cdms.json"
# with open(config_st_output, 'w', encoding='utf-8') as f:
#     json.dump(data, f, ensure_ascii=False, indent=4)
