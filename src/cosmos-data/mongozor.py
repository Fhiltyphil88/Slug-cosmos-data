from pymongo import MongoClient
from config import DB, DB_COLS


def get_database_full_url():
    mongo_host = DB["host"]+":"+DB["port"]
    mongo_usercreds = DB["user"]+":"+DB["pass"]

    return "mongodb://"+mongo_usercreds+"@"+mongo_host+"/"


def get_database_client():
    """This function returns a MongoClient object to interact with a mongodb
    database named in configuration

       Configuration is read to connect with the correct database URL and
       credentials. The database name is defined in the configuration.
       See import config (from config.py)
    """
    db_url = get_database_full_url()
    print(db_url)
    # Create a connection to DB using MongoClient.
    client = MongoClient(db_url)
    print(client)

    # Create a database with client["DB_name"]
    # using database name from configuration so it is always the same
    return client[DB["name"]]


def insert_cdms(cdms_array):
    """ Insert an array of CDMs into mongoDB corresponding collection

        :param cdms_array: array of CDMs to save into mongo database
    """
    database = get_database_client()
    sd_sdm = database[DB_COLS["cdm"]]

    # --- Check if index is well set in sd_cdm collection
    if "CDM_ID_1" not in sd_sdm.index_information():
        sd_sdm.create_index("CDM_ID", unique=True)
        print(sd_sdm.index_information())

    # --- Insert data
    # data = database[DB_COLS["cdm"]].insert_one({"g":"owesome"})
    # print(data.inserted_id)
    data = sd_sdm.insert_many(cdms_array)

    return data


def insert_rso(from_cdm: dict):
    """ Insert both RSO of a CDM into database

        :param from_cdm: CDM data used to create RSOs

    Typical content of a CDM (from Space Track)
        "CDM_ID": "445493215",
        "CREATED": "2023-02-25 22:09:53.000000",
        "EMERGENCY_REPORTABLE": "Y",
        "TCA": "2023-02-28T15:45:44.261000",
        "MIN_RNG": "3376",
        "PC": null,
        "SAT_1_ID": "503",
        "SAT_1_NAME": "RELAY 1 (A-15)",
        "SAT1_OBJECT_TYPE": "PAYLOAD",
        "SAT1_RCS": "MEDIUM",
        "SAT_1_EXCL_VOL": "0.00",
        "SAT_2_ID": "40162",
        "SAT_2_NAME": "CZ-3B DEB",
        "SAT2_OBJECT_TYPE": "DEBRIS",
        "SAT2_RCS": "MEDIUM",
        "SAT_2_EXCL_VOL": "0.00",
        "SUPPLIED_ID": "st001",
        "EVENT_ID": "50340162",
        "WHERE_LAT": null,
        "WHERE_LON": null
    """
    database = get_database_client()
    sd_rso = database[DB_COLS["rso"]]

    # --- Check if index is well set in rso collection
    if "RSO_ID_1" not in sd_rso.index_information():
        sd_rso.create_index("RSO_ID", unique=True)
        print(sd_rso.index_information())

    # --- Add both RSO objects to database
    for k in ["1", "2"]:
        rso = {}
        sat = "SAT_"+k+"_"
        rso["NAME"] = from_cdm[sat+"NAME"]
        rso["NORAD_ID"] = from_cdm[sat+"ID"]
        rso["TYPE"] = from_cdm[sat+"OBJECT_TYPE"]

        # computed fields
        if rso["TYPE"] == "DEBRIS":
            rso["MANOEUVRABLE"] = False
        else:
            rso["MANOEUVRABLE"] = None

        # additional fields from ext. sources
        rso["ACTIVE"] = None
        rso["PUBLIC"] = None

        try:
            data = sd_rso.insert_one(rso)
        except Exception as eee:
            print(eee)
            print("EEE remediation: do nothing.\n\t" +
                  "RSO could not be added/already exists: {}".format(rso))
        return data

# import json
# with open('../../__data__sd_all_cdms.json', 'r') as fd:
#     cdms_data = json.load(fd)
#     print(len(cdms_data))
#     insert_cdms(cdms_data)
